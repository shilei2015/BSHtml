
/*--------------------------------------SLNetwork------------------------------------------------*/
//列表数据加载
/*
* rqType:请求方式 post / get
* apistr:接口短链接
* params:请求参数
* cb:回调函数
* addMore:是否加载更多（ture:加载更多  false:加载第一页或者）
* */

let SLNetwork = new function () {
    z = this;

    // var baseApi = 
    // var baseApi = "";

    z.baseApi = function () {

        if (location.href.indexOf("http") >= 0) {
            if (location.href.indexOf("1v1agent") >= 0) {
                return "http://1v1agent-api.cookiegeeks.com/agent/";
            } else {
                return "http://agent-api.dates.fit/agent/"
            }
        } else {
            return "http://1v1agent-api.cookiegeeks.com/agent/";
        }

    }



    z.POST = function (apistr, params, cb, loading) {
        Request("POST", apistr, params, cb, loading);
    };

    z.GET = function (apistr, params, cb, loading) {
        Request("GET", apistr, params, cb, loading);
    };


    var Request = function (rType, apistr, params, cb, loading = false) {

        params = params || {};
        var newParms = params;

        let config = {
            method: rType,
            headers: {
                "Content-Type": "application/json",
                "Token": Storage.Token,
            },
            // model:'no-cors',
        };

        config["Access-Control-Allow-Origin"] = "*";

        if (rType == "POST") {
            config.body = JSON.stringify(params);
        }

        if (loading) {
            Loading.show(loading)
        }


        var api = "";
        if (apistr.indexOf('http') <= -1) {
            api = SLNetwork.baseApi() + apistr;
        } else {
            api = apistr;
        }

        fetch(api, config)
            .then((response) => response.json())
            .then((data) => {

                if (loading) { Loading.show(false) }
                if (data.code && data.code == 0) {

                } else {
                    Loading.tip(data.data.msg);

                    if (data.code == 1) {
                        // setTimeout(function(params) {
                        exitLogin();
                        // },1000);
                        return;
                    }
                }

                if (cb) { cb(data) }
            })
            .catch((error) => {
                // console.log("Error:" + error.message);
                if (loading) { Loading.show(false) }
                Loading.tip(error);
                return;
                if (cb) {
                    cb({
                        code: -1,
                        data: {},
                    })
                }
            });

    };



    //加载层
    var Loading = {
        box: null,
        init: function () {
            if (Loading.box === null) {
                Loading.box = document.createElement('div');
                Loading.box.id = 'loadingBG';
                document.body.appendChild(Loading.box);
            }
        },
        show: function (arg) {
            Loading.init();
            if (arg == null || !arg) {
                $(Loading.box).hide();
            } else {
                if (arg == true || arg == '') arg = 'Loading ...';
                $(Loading.box).html('<div class="loading"><div class="spinner-border"></div><div>' + arg + '</div></div>');
                $(Loading.box).show();
            }
        },

        tip: function (arg) {
            Loading.init();
            if (arg == null || !arg) {
                $(Loading.box).hide();
            } else {
                if (arg == true || arg == '') arg = 'Loading ...';
                $(Loading.box).html('<div class="loading"><div>' + arg + '</div></div>');
                $(Loading.box).show();

                setTimeout(function () {
                    $(Loading.box).hide();
                }, 1000);

            }
        },

    };

    z.loading = Loading;

}





let Storage = new function () {

    z = this;

    z.save = function (key, obj) {
        localStorage.setItem(key, JSON.stringify(obj));
    };

    z.get = function (key) {
        let getJson = localStorage.getItem(key);
        let getP = JSON.parse(getJson);

        return getP
    };

    z.remove = function (key) {
        localStorage.removeItem(key);
    };

    z.clear = function () {
        localStorage.clear();
    }

    z.kUser = "user";


    z.User = z.get(z.kUser);

    z.Token = z.User ? z.User.Token : "";

};









/**




    fetch("https://example.com/profile", {
    method: "POST", // or 'PUT'
    headers: {
    "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
    })
    .then((response) => response.json())
    .then((data) => {
    console.log("Success:", data);
    })
    .catch((error) => {
    console.error("Error:", error);
    });


    // Example POST method implementation:
    async function postData(url = "", data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    mode: "cors", // no-cors, *cors, same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    headers: {
    "Content-Type": "application/json",
    // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: "follow", // manual, *follow, error
    referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin,
    same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data), // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
    }

    postData("https://example.com/answer", { answer: 42 }).then((data) => {
    console.log(data); // JSON data parsed by `data.json()` call
    });



*/





/*--------------------------------------network 使用方式------------------------------------------------*/

/*

<script id="" type="text/html">

//循环
{{each $data as model i}}
{{/each}}


//条件判断
{{if model.face}}
    <img src={{model.face}} alt="">
{{else}}
    <img src="img/tempIcon/banner.png" alt="">
{{/if}}

</script>




var list = data.data;
var html = template('banner_id', list);
document.getElementById('list_id_banner').innerHTML = html;

*/

function network() {
    var params = {}
    SLNetwork('get/post', API_, params, function (data) {
        if (data.code == 1) {
            Comm.message('接口调用成功');
        } else {
            Comm.message(data.msg);
        }
    });
}

function getListElementWithArr_TT(arr) {

    var list = '';
    for (var i = 0; arr && arr.length > 0 && i < arr.length; i++) {
        list += '';
    }
    return list;
}

function dealWith() {
    var arr = [];
    for (var i = 0; i < arr.length; i++) {
        var model = arr[i];
    }
}

